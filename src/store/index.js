import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import cors from 'cors'

Vue.use(Vuex)

Vue.use(cors)

const instance = axios.create({baseURL: 'http://127.0.0.1:8081'
})
const store = new Vuex.Store({
  state:{
    attrs: []
  },
  getters:{
    attrs: state => state.attrs
  },
  mutations:{
    setAttrs (state, attrs ) {
      state.attrs = attrs
      console.log('attributes set')
    }
  },
  actions: {
    async loadAttrs ({commit}){
      // let bands = [{
      //   id: 1,
      //   name: 'Somename'
      // },{
      //   id: 2,
      //   name: 'Somewsfqewfeothernaaaaame'
      // }]
      console.log('loading attrs')
      let data = new FormData();
      data.append('someparam', 'somevalue')
      //data.append('someotherparam', 'somevalue')
      let attrs = instance.post('/api/attrs', data)
        .then(r => r.data)
        .then(result => {
          commit('setAttrs', result)
        })
      console.log(attrs)
    }
  }
})

export default store
